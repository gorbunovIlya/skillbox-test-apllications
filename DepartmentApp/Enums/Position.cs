﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DepartmentApp.Enums
{
    public enum Position
    {
        Director,
        DepartmentHead,
        BranchHead,
        Worker,
        Intern

    }
}
