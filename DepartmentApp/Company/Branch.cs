﻿using System;
using System.Collections.Generic;
using System.Text;
using DepartmentApp.Staff;

namespace DepartmentApp.Company
{
    public class Branch : BaseCompany
    {
        public Worker BranchHead { get; set; }
        public List<Worker> Workers { get; set; }
    }
}
