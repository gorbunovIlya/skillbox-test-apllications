﻿using System;
using System.Collections.Generic;
using System.Text;
using DepartmentApp.Staff;

namespace DepartmentApp.Company
{
    public class Department : BaseCompany
    {
        public Worker Chief { get; set; }
        public List<Branch> Branches { get; set; }
    }
}
