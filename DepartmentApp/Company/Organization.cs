﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DepartmentApp.Staff;

namespace DepartmentApp.Company
{
    public class Organization : BaseCompany
    {
        public Worker Director { get; set; }
        public List<Department> Departments { get; set; }

        public override string ToString()
        {
            return $"Название организации: {Name}\n" +
                   $"| Директор: {Director.Name} {Director.Surname}\n" +
                   $"| Количество департаментов: {Departments.Count}\n" +
                   $"| Количество отделов: {Departments.SelectMany(c => c.Branches).Count()}\n" +
                   $"| Количество работников: {Departments.SelectMany(c => c.Branches).SelectMany(c => c.Workers).Count()}";
        }
    }

}
