﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using DepartmentApp.Company;
using DepartmentApp.Enums;
using DepartmentApp.FileManager;
using DepartmentApp.Staff;

namespace DepartmentApp
{
    public class Program
    {
        

        public static void Main()
        {
            List<Worker> workers = JsonSerializer
                .Deserialize<List<Worker>>(FileReader.GetJson($@"{Environment.CurrentDirectory}\workers.json"));

            List<Department> departments = JsonSerializer
                .Deserialize<List<Department>>(FileReader.GetJson($@"{Environment.CurrentDirectory}\departments.json"));

             Organization organization = new Organization(){
                 Departments = departments,
                 Name = "Organization",
                 Director = workers.FirstOrDefault(c => c.Position == Position.Director)
             };
             while (true)
             {
                 GetSystem(organization);
            }
        }

        public static void GetSystem(Organization organization)
        {
            Console.WriteLine("\t\t\t---Информационная система---\n");
            Console.WriteLine("Выберите действие:\n1 - Поиск сотрудника\n2 - Информации об организации");
            string result = Console.ReadLine();
            if (result == "1" || result == "2")
            {
                ActionHandler(Convert.ToInt32(result), organization);
            }
            else
            {
                Console.WriteLine("Неизвестная команда");
                GetSystem(organization);
            }
        }

        public static void ActionHandler(int result, Organization organization)
        {
            switch (result)
            {
                case 1:
                    Console.WriteLine("Введите Ф.И. сотрудника\n");
                    string fullName = Console.ReadLine()?.ToLower();
                    var employee = organization.Departments
                        .SelectMany(c => c.Branches)
                        .SelectMany(c => c.Workers)
                        .FirstOrDefault(c =>
                        c.Name.ToLower() == fullName);
                    
                    Console.WriteLine("\t\t\t--Результат поиска---\n");
                    if (employee != null)
                    {
                        Console.WriteLine(employee.ToString());
                        Console.WriteLine(@"Расчитать зарплату Да\Нет?");
                        string answer = Console.ReadLine()?.ToLower();
                        switch (answer)
                        {
                            case "да":
                                switch (employee.Position)
                                {
                                    case Position.Intern:
                                        Console.WriteLine("Заработная плата интерна составляет 500$");
                                        break;
                                    case Position.Worker:
                                        Console.WriteLine("Заработная плата рабочего составляет {0}$", 22 * 8 * 12);
                                        break;
                                    case Position.BranchHead:
                                        double salary = organization.Departments
                                            .SelectMany(c => c.Branches)
                                            .Where(c => c.BranchHead == employee)
                                            .SelectMany(c => c.Workers)
                                            .Sum(c => c.Salary);
                                        Console.WriteLine("Заработная плата начальника подразделния составляет {0}$",salary * 22 * 0.15) ;
                                        break;
                                    case Position.DepartmentHead:
                                        salary = organization.Departments
                                            .Where(c => c.Chief == employee)
                                            .SelectMany(c => c.Branches)
                                            .SelectMany(c => c.Workers)
                                            .Sum(c => c.Salary);
                                        Console.WriteLine("Заработная плата начальника департамента составляет {0}$", salary * 22 * 0.15);
                                        break;
                                    case Position.Director:
                                        salary = organization.Departments.SelectMany(c => c.Branches)
                                            .SelectMany(c => c.Workers).Sum(c => c.Salary);
                                        Console.WriteLine("Заработная плата директора составляет {0}$", salary * 22 * 0.15);
                                        break;
                                }
                                break;
                            case "нет":
                                GetSystem(organization);
                                break;
                            default: Console.WriteLine("неизвестная команда");
                                GetSystem(organization);
                                break;
                        }
                    }
                    else { Console.WriteLine("Ничего не найдено"); }
                    break;
                case 2:
                {
                    Console.WriteLine(organization.ToString());
                    break;
                }
            }
        }
    }
}
