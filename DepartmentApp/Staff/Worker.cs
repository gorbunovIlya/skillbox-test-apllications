﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DepartmentApp.Company;
using DepartmentApp.Enums;

namespace DepartmentApp.Staff
{
    public class Worker
    {
        //public Organization Organization { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public Position Position { get; set; }
        public double Salary { get; set; }
        //public Worker()
        //{
            //if (this.Position == Position.Intern)
            //{
            //    this.Salary = 500;
            //}

            //if (this.Position == Position.Worker)
            //{
            //    this.Salary = 22 * 8 * 12;
            //}
            //if (this.Position == Position.Director)
            //{
            //    this.Salary = this.Organization.Departments
            //        .SelectMany(c => c.Branches)
            //        .SelectMany(c => c.Workers).Sum(c => c.Salary) * 22 * 0.15;
            //}
            //if (this.Position == Position.DepartmentHead)
            //{
            //    this.Salary = Organization.Departments
            //                      .Where(c => c.Chief == this)
            //                      .SelectMany(c => c.Branches)
            //                      .SelectMany(c => c.Workers)
            //                      .Sum(c => c.Salary) * 22 * 0.15;
            //}

            //if (this.Position == Position.BranchHead)
            //{
            //    this.Salary = Organization.Departments
            //                       .SelectMany(c => c.Branches)
            //                       .Where(c => c.BranchHead == this)
            //                       .SelectMany(c => c.Workers)
            //                       .Sum(c => c.Salary) * 22 * 0.15;
            //}
        //}
        public override string ToString()
        {
            return $"Имя: {Name} | Фамилия: {Surname} | Должность: {Position}";
        }
    }
}
