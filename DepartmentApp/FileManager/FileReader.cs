﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DepartmentApp.FileManager
{
    public static class FileReader
    {
        private static StreamReader _reader;
        public static string GetJson(string path)
        {
            _reader = new StreamReader(path);
            string file = _reader.ReadToEnd();
            _reader.Close();
            _reader.Dispose();
            return file;
        }
    }
}
