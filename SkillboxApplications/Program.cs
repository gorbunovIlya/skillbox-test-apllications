﻿using System;

namespace SkillboxApplications
{
    class Program
    {
        static void Main(string[] args)
        {
            var bot = TelegramBot.Get();
            bot.OnMessage += MessageManager.MessageListenerAsync;
            bot.StartReceiving();
            Console.WriteLine("Бот запущен");
            Console.ReadLine();
        }
    }
}
