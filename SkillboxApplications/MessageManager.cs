﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;

namespace SkillboxApplications
{
    public static class MessageManager
    {
        private static readonly TelegramBotClient _bot = TelegramBot.Get();
        public static async void MessageListenerAsync(object sender, MessageEventArgs e)
        {
            try
            {
                string replyMessage;
                switch (e.Message.Type)
                {
                    case MessageType.Text:
                        switch (e.Message.Text)
                        {
                            case "/givepicture":
                                await _bot.SendPhotoAsync(e.Message.Chat.Id,
                                    new InputOnlineFile("https://cdn2.thecatapi.com/images/ac7.jpg"));
                                return;
                            case "/givedocument":
                                await _bot.SendDocumentAsync(e.Message.Chat.Id,
                                    new InputOnlineFile("https://www.xeroxscanners.com/downloads/Manuals/XMS/PDF_Converter_Pro_Quick_Reference_Guide.RU.pdf"));
                                return;
                            default:
                                await _bot.SendTextMessageAsync(e.Message.Chat.Id,
                                    replyToMessageId: e.Message.MessageId, text: e.Message.Text);
                                return;
                        }
                    case MessageType.Document:
                        DownloadAsync(e.Message.Document.FileId, e.Message.Document.FileName);
                        replyMessage = "Документ успешно скачан";
                        break;

                    case MessageType.Audio:
                        DownloadAsync(e.Message.Audio.FileId, $"{e.Message.Audio.Performer} - " +
                                                              $"{e.Message.Audio.Title}." +
                                                              $"{e.Message.Audio.MimeType.Split("/")[1]}");
                        replyMessage = "Аудио успешно скачано";
                        break;

                    case MessageType.Video:
                        DownloadAsync(e.Message.Video.FileId, $"{e.Message.Video.FileId}." +
                                                              $"{e.Message.Video.MimeType.Split("/")[1]}");
                        replyMessage = "Видео успешно скачано";
                        break;

                    case MessageType.Photo:
                        foreach (var photo in e.Message.Photo)
                        {
                            DownloadAsync(photo.FileId, photo.FileId);
                        }

                        replyMessage = "Фото успешно скачано";
                        break;

                    default:
                        replyMessage = "Я пока не умею сохранять эти файлы " + char.ConvertFromUtf32(0x1F61E);
                        break;

                }
                await _bot.SendTextMessageAsync(e.Message.Chat.Id, replyMessage, replyToMessageId: e.Message.MessageId);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
            }

        }
        static async void DownloadAsync(string fileId, string fileName)
        {
            var file = await _bot.GetFileAsync(fileId);
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            await using FileStream fs = new FileStream($@"{path}\{fileName}", FileMode.Create);
            await _bot.DownloadFileAsync(file.FilePath, fs);

        }
    }
}
