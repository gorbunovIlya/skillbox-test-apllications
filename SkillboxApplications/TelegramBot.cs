﻿using System;
using System.Collections.Generic;
using System.Text;
using Telegram.Bot;

namespace SkillboxApplications
{
    public class TelegramBot
    {
        private static TelegramBotClient _client;

        public static TelegramBotClient Get()
        {
            if (_client != null)
            {
                return _client;
            }

            _client = new TelegramBotClient(AppSettings.Key);

            return _client;
        }
    }
}
