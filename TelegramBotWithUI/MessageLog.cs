﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TelegramBotWithUI
{
    public class MessageLog
    {
        public string Time { get; set; }
        public string Name { get; set; }
        public long Id { get; set; }
        public string Message { get; set; }

        public MessageLog(string time, string name, long id, string message)
        {
            Time = time;
            Message = message;
            Name = name;
            Id = id;
        }
    }
}
