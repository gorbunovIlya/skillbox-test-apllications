﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace TelegramBotWithUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MessageClient client;
        public MainWindow()
        {
            InitializeComponent();
            client = new MessageClient(this);
            logList.ItemsSource = client.Logs;
        }
        private void ButtonSendMessage_OnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TargetSend.Text))
            {
                client.SendMessageAsync(TargetSend.Text, textMessageSend.Text);
            }
            else
            {
                MessageBox.Show("Не выбран пользователь", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TargetSend.Text))
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.Multiselect = false;
                if (fileDialog.ShowDialog() == true)
                {
                    client.SendFiles(TargetSend.Text, fileDialog.FileName, fileDialog.SafeFileName);
                }
            }
            else
            {
                MessageBox.Show("Не выбран пользователь", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
    }
}
