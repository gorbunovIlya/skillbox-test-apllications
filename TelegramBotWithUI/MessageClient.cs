﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using NLog;
using SkillboxApplications;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;

namespace TelegramBotWithUI
{
    public class MessageClient
    {
        private MainWindow w;
        private readonly TelegramBotClient _bot = TelegramBot.Get();
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public ObservableCollection<MessageLog> Logs { get; set; }

        public MessageClient(MainWindow w)
        {
            _bot.OnMessage += MessageListener;
            _bot.StartReceiving();
            this.w = w;
            Logs = new ObservableCollection<MessageLog>();
        }
        private void MessageListener(object sender, MessageEventArgs e)
        {
            try
            {
                switch (e.Message.Type)
                {
                    case MessageType.Text:
                        w.Dispatcher?.Invoke(() => Logs.Add(
                            new MessageLog(DateTime.Now.ToLongDateString(),
                                e.Message.Chat.FirstName, e.Message.Chat.Id, e.Message.Text)));
                        logger.Info($"Имя: {e.Message.Chat.FirstName} | " +
                                    $"Сообщение: {e.Message.Text} ");
                        break;
                    default:
                        SendMessageAsync(e.Message.Chat.Id.ToString(), "Неизвестный формат команды");
                        break;

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                Debug.WriteLine(ex.Message);
            }
        }
        public async void SendMessageAsync(string id, string message)
        {
            await _bot.SendTextMessageAsync(Convert.ToInt64(id), message);
        }

        public async void SendFiles(string id, string value, string fileName)
        {
            await _bot.SendDocumentAsync(Convert.ToInt64(id),
                new InputOnlineFile(new MemoryStream(File.ReadAllBytes(value)), fileName));
            MessageBox.Show("Файл отправлен");
        }
    }
}
